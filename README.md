# graphclj

On peut appeler le programme sans arguments. Dans ce
cas, un graphe aléatoire est créé puis celui est affiché avec les informations
internes ainsi qu'avec la représentation to-dot.

##Exemple de commande :

--> lein run -src rand -p 0.5 -size 5

=> permet de créer un graphe généré aléatoirement
avec comme taille 5 et comme facteur random p 0.5

--> lein run -src data/graph.graph

=> Permet de créer un graphe généré a partir de la
définition du fichier graph.graph situé dans le dossier data.
le fichier doit finir par .graph pour etre reconnu comme un fichier de graphe.


##Commandes possibles

-src : fournir rand ou path le chemin d'un fichier .graph

-size : sert a la génération d'un graphe aléatoire en lui donnant
un nombre de sommets

-p : sert a la génération d'un graphe aléatoire en lui donnant son
paramètre p. (doit être entre 0 et 1)

-l : peut être :close ou :degree    sert a choisit la manière dont sont triés les aretes par ordre d'importance.

La validite de la string au format .dot a été testée avec Graphviz Online

## bugs connus :
La creation d'un graphe avec 0 arêtes.

## License

Copyright © 2019 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
