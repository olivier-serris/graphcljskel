(ns graphclj.tools
  (:require [clojure.string :as str]))

(defn readfile [f]
    "Returns a sequence from a file f"
    (with-open [rdr (clojure.java.io/reader f)]
            (doall (line-seq rdr))))

(defn rank-nodes [g,l]
  "Ranks the nodes of the graph in relation to label in accending order"
  (->> g
    (group-by (fn [[node desc]] (get desc l)))
    (sort-by first <)
    (map-indexed (fn [i [label nodes]]
                     (map (fn [[node desc]]
                            [node (assoc desc :rank i)])
                       nodes)))
    (apply concat)
    (into {})))

(defn generate-colors [n]
    (let [step 10]
     (loop [colors {}, current [255.0 160.0 122.0], c 0]
       (if (= c (inc n))
         colors
         (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
                (map #(mod (+ step %) 255) current) (inc c))))))

(defn graph-to-edges [g]
  " take a graph and return a set of pair of vertex to represent edges"
  (->> g
    (mapcat (fn [[node desc]]
              (map (fn [neigh] #{node neigh}) (get desc :neigh))))
    (into #{})))

(defn edges-to-string [edges]
  "convert collection of edges into dot string edges"
  (->> (map seq edges)
    (map (fn [[a b]] (str a " -- " b "\n")))
    (reduce str)))

(defn graph-to-color[g]
  "convert graphe into dot format color nodes"
  (let [ranks (map (fn [n] (get-in n [1 :rank])) g)
        max-rank (reduce max ranks)
        colors (generate-colors max-rank)
        graph-color (map (fn [[node desc]] [node (get colors (:rank desc))]) g)
        graph-color (map (fn [[node color]]
                           (str "\n" node " [style=filled color=\"" (reduce str color) "\"]"))
                      graph-color)
        graph-color (reduce str graph-color)]
    graph-color))

(defn to-dot [g]
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
  (let [node-color (graph-to-color g)]
    (str "graph g{" node-color  "\n"(edges-to-string (graph-to-edges g))"}")))
