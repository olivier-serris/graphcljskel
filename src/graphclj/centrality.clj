(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))

(defn degrees [g]
  "Calculates the degree centrality for each node"
  (reduce (fn [graph [id desc]]
            (assoc graph id (assoc desc :degree (count (:neigh desc)))))
      {} g))


(defn distance [g n]
  "Calculate the distances of one node to all the others"
  (let [children (:neigh (get g n))]
    (loop [res {n 0} s-children children grand-children #{} dist 1.0]
      (if (seq s-children)
        (let [child (first s-children)]
          (recur (assoc res child dist)
                 (rest s-children)
                 (set/union grand-children (:neigh (get g child)))
                 dist))
        (if (seq grand-children)
          (recur res
                 (set/difference grand-children (into #{} (keys res)))
                 #{}
                 (inc dist))
          res)))))
(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  (reduce (fn [sum [node d]] (+ sum (/ 1 d))) 0 (dissoc (distance g n) n)))


(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  ;(map (fn [node] [node (closeness g node)]) (keys g)))
  (reduce (fn [accu [node desc]]
            (assoc accu node
              (assoc desc :close (closeness g node))))
        {} g))
