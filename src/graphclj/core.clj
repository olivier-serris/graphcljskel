(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central]
            [clojure.string :as str])
  (:gen-class))

(declare parse-args)
(declare get-graph)

(defn -main
  [& args]
  (try
    (let [arg-map (parse-args args)]
      (println "args are : " arg-map)
      (let [g (get-graph arg-map)
            label (get arg-map :sort :close)
            g (central/closeness-all g)
            g (tools/rank-nodes g label)
            dot-g (tools/to-dot g)]
        (println "graphe " (:src arg-map "rand")": \n")
        (println (reduce (fn [accu x] (str accu "\n" x)) "" g))
        (println "\nto-dot : \n" dot-g)))
    (catch Exception e
      (println "caught exception: " (.getMessage e)))))


(defn parse-args[args]
  (loop [res {} s-args args]
    (if (and (seq s-args) (seq (rest  s-args)))
      (let [key (first s-args)
            arg (first (rest s-args))]
        (case key
          "-src" (recur (assoc res :src arg) (rest (rest s-args)))
          "-size" (recur (assoc res :size (read-string arg)) (rest (rest s-args)))
          "-p" (recur (assoc res :p (read-string arg)) (rest (rest s-args)))
          "-sort" (recur (assoc res :sort arg) (rest (rest s-args)))
          :default (println arg " not recognized")))
      res)))

(defn get-graph [arg-map]
  (let [graph-type (get arg-map :src "rand")
        size  (get arg-map :size 5)
        p (get arg-map :p 0.5)]
       (cond
         (str/ends-with? graph-type ".graph")
         (graph/gen-graph (tools/readfile graph-type))

         (= graph-type "rand")
         (graph/erdos-renyi-rnd size p)

         :else (graph/erdos-renyi-rnd size p))))
