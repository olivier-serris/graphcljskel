(ns graphclj.graph
  (:require [clojure.string :as str]))


(defn add-oriented-edge [graph edge]
  (let [[s1 s2] (map #(Integer/parseInt %) (str/split edge #" "))]
    (let [info (if (contains? graph s1)
                 {:neigh  (conj  (:neigh (get graph s1)) s2)}
                 {:neigh #{s2}})]
      (assoc graph s1 info))))

(defn gen-graph [lines]
    "Returns a hashmap contating the graph"
    (let [reverse-lines (map (fn [x] (apply str (reverse x))) lines)]
      (reduce add-oriented-edge {} (concat lines reverse-lines))))

(defn get-pair [coll]
  "Return all pair from a collection"
  (if (not (empty? coll))
    (concat
      (map (fn [x] #{(first coll) x}) (rest coll))
      (get-pair (rest coll)))
    ()))
    
(defn erdos-renyi-rnd [n,p]
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
  (let [nodes (take n (range))
        all-edges (get-pair nodes)
        lines (filter (fn [x] (< (rand) p)) all-edges);; filter vertices by probability of having them
        lines (mapcat (fn [edge] [(str (first edge) " " (second edge))]) lines)]
    (gen-graph lines)))
